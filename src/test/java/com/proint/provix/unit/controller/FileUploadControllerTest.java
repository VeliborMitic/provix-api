package com.proint.provix.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proint.provix.security.UserCredentials;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class FileUploadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final Logger logger = LoggerFactory.getLogger(FileUploadControllerTest.class);
    private static final String BASE_URI = "/api/v1/applicants";
    private static final String SLASH_ID = "/{id}";
    private static final String FILES_URI = "/files";
    private MockMultipartFile firstFile;
    private MockMultipartFile secondFile;
    private MockMultipartFile thirdFile;
    private MockMultipartFile fourthFile;
    private MockMultipartFile fifthFile;
    private MockMultipartFile sixthFile;
    private static final String FILES = "files";
    private static final Integer NON_EXISTING_APPLICANT_ID = Integer.MAX_VALUE;
    private MockMultipartFile notAllowedExtensionFile;


    @BeforeEach
    void setUp() {
        firstFile = new MockMultipartFile
                (FILES, "first.pdf", "application/pdf", "first pdf".getBytes());
        secondFile = new MockMultipartFile
                (FILES, "second.jpeg", "image/jpeg", "second jpeg".getBytes());
        thirdFile = new MockMultipartFile
                (FILES, "third.doc", "application/msword", "third doc".getBytes());
        fourthFile = new MockMultipartFile
                (FILES, "fourth.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "fourth docx".getBytes());
        fifthFile = new MockMultipartFile
                (FILES, "fifth.odt", "application/vnd.oasis.opendocument.text",
                        "fifth odt".getBytes());
        sixthFile = new MockMultipartFile
                (FILES, "sixth.zip", "application/zip", "sixth pdf".getBytes());
        notAllowedExtensionFile = new MockMultipartFile
                (FILES, "seventh.mpeg", "video/mpeg", "seventh pdf".getBytes());
    }

    @Test
    @DisplayName("Return '200 OK' when files are uploaded")
    void testUploadFilesOk() {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart(BASE_URI + SLASH_ID + FILES_URI, 1)
                .file(firstFile)
                .file(secondFile)
                .file(thirdFile)
                .file(fourthFile)
                .file(fifthFile);
        try {
            this.mockMvc.perform(builder)
                    .andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when replacing the file with same name and path")
    void testUploadFilesOverwritingExistingFileOk() {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart(BASE_URI + SLASH_ID + FILES_URI, 1)
                .file(firstFile)
                .file(firstFile);
        try {
            this.mockMvc.perform(builder)
                    .andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 BAD REQIEST' when uploading more then 5 files")
    void testUploadFilesBadRequest() {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart(BASE_URI + SLASH_ID + FILES_URI, 1)
                .file(firstFile)
                .file(secondFile)
                .file(thirdFile)
                .file(fourthFile)
                .file(fifthFile)
                .file(sixthFile);
        try {
            this.mockMvc.perform(builder)
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 BAD REQUEST' when uploading not allowed mime type file")
    void testUploadFilesNotAllowedExtension() {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart(BASE_URI + SLASH_ID + FILES_URI, 1)
                .file(notAllowedExtensionFile);
        try {
            this.mockMvc.perform(builder)
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 Ok' when retrieving all Applicant files")
    void testGetApplicantsByPositionIdNotFound() {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get(BASE_URI + SLASH_ID + FILES_URI, 1)
                    .header(HttpHeaders.AUTHORIZATION, authenticate()))

                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when retrieving all Applicant files with non existing Applicant Id")
    void testGetFilesByApplicantIdNotFound() {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get(BASE_URI + SLASH_ID + FILES_URI, NON_EXISTING_APPLICANT_ID)
                    .header(HttpHeaders.AUTHORIZATION, authenticate()))

                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    String authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@provix.com");
        userCredentials.setPassword("Qwerty123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            return mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
        return null;
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}