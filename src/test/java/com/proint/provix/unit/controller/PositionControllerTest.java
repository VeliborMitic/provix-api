package com.proint.provix.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proint.provix.entity.Position;
import com.proint.provix.model.position.PositionPostModel;
import com.proint.provix.security.UserCredentials;
import com.proint.provix.service.PositionService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class PositionControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(PositionControllerTest.class);
    private static final String NAME = "Disc Jockey";
    private static final String DESCRIPTION = "Description";
    private static final String START_DATE = "2019-05-08T12:47:27.985Z";
    private static final String END_DATE = "2019-06-08T12:47:27.985Z";
    private static final String POSITIONS_URI = "/api/v1/positions";
    private static final String POSITIONS_URI_ID = POSITIONS_URI + "/{id}";
    @MockBean
    private PositionService positionService;

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private MockMvc mockMvc;
    private String token;
    private Position position;
    private PositionPostModel positionPostModel;

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> positionStream() {
        PositionPostModel positionWithoutName = new PositionPostModel();
        positionWithoutName.setDescription(DESCRIPTION);
        positionWithoutName.setStartDate(START_DATE);
        positionWithoutName.setEndDate(END_DATE);

        PositionPostModel positionWithoutDescription = new PositionPostModel();
        positionWithoutDescription.setName(NAME);
        positionWithoutDescription.setStartDate(START_DATE);
        positionWithoutDescription.setEndDate(END_DATE);

        PositionPostModel positionWithoutStartDate = new PositionPostModel();
        positionWithoutStartDate.setName(NAME);
        positionWithoutStartDate.setDescription(DESCRIPTION);
        positionWithoutStartDate.setEndDate(END_DATE);

        PositionPostModel positionWithoutEndDate = new PositionPostModel();
        positionWithoutEndDate.setName(NAME);
        positionWithoutEndDate.setDescription(DESCRIPTION);
        positionWithoutEndDate.setStartDate(START_DATE);

        return Stream.of(
                Arguments.of(positionWithoutName),
                Arguments.of(positionWithoutDescription),
                Arguments.of(positionWithoutStartDate),
                Arguments.of(positionWithoutEndDate)
        );
    }

    @BeforeAll
    void authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@provix.com");
        userCredentials.setPassword("Qwerty123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
    }

    @BeforeEach
    void initialData() {
        positionPostModel = new PositionPostModel();
        positionPostModel.setName(NAME);
        positionPostModel.setDescription(DESCRIPTION);
        positionPostModel.setStartDate(START_DATE);
        positionPostModel.setEndDate(END_DATE);

        position = new Position();
        position.setId(1);
        position.setName(NAME);
        position.setDescription(DESCRIPTION);
        position.setStartDate(Instant.parse(START_DATE));
        position.setEndDate(Instant.parse(END_DATE));
    }

    @Test
    @DisplayName("Return 'OK' when position exists.")
    void getPositionOk() {
        when(positionService.get(1)).thenReturn(Optional.of(position));

        try {
            mockMvc.perform(MockMvcRequestBuilders.get(POSITIONS_URI_ID, 1)
                    .header(HttpHeaders.AUTHORIZATION, token))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'Not Found' when position does not exist.")
    void getPositionNotFound() {
        doReturn(Optional.empty()).when(positionService).get(1);

        try {
            mockMvc.perform(MockMvcRequestBuilders.get(POSITIONS_URI_ID, 1)
                    .header(HttpHeaders.AUTHORIZATION, token))
                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'Created' when position is created")
    void createPositionCreated() {
        Position mappedPosition = modelMapper.map(positionPostModel, Position.class);
        mappedPosition.setId(1);

        doReturn(Optional.of(mappedPosition)).when(positionService).create(any());

        try {
            mockMvc.perform(MockMvcRequestBuilders.post(POSITIONS_URI)
                    .header(HttpHeaders.AUTHORIZATION, token)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(positionPostModel)))
                    .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'OK' when retrieving all activities")
    void getAllPositionsOk() {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get(POSITIONS_URI)
                    .header(HttpHeaders.AUTHORIZATION, token))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @ParameterizedTest
    @MethodSource("positionStream")
    @DisplayName("Return 'Bad Request' when request does not contain valid body, i.e. invalid object")
    void createPositionBadRequest(PositionPostModel positionPostModel) {
        Position mappedPosition = modelMapper.map(positionPostModel, Position.class);
        mappedPosition.setId(1);
        doReturn(Optional.of(mappedPosition)).when(positionService).create(mappedPosition);

        try {
            mockMvc.perform(MockMvcRequestBuilders.post(POSITIONS_URI)
                    .content(asJsonString(positionPostModel))
                    .header(HttpHeaders.AUTHORIZATION, token)
                    .header(HttpHeaders.CONTENT_TYPE, "application/json"))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'Conflict' on attempting to create a position with duplicate name")
    void createPositionConflict() {
        Position mappedPosition = modelMapper.map(positionPostModel, Position.class);

        doReturn(Optional.of(mappedPosition)).when(positionService).getByName(positionPostModel.getName());

        try {
            mockMvc.perform(MockMvcRequestBuilders.post(POSITIONS_URI)
                    .content(asJsonString(positionPostModel))
                    .header(HttpHeaders.AUTHORIZATION, token)
                    .header(HttpHeaders.CONTENT_TYPE, "application/json"))
                    .andExpect(MockMvcResultMatchers.status().isConflict());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'OK' when deleting activity")
    void deleteActivityNoContent() {
        when(positionService.get(1)).thenReturn(Optional.of(position));

        try {
            mockMvc.perform(MockMvcRequestBuilders.delete(POSITIONS_URI_ID, 1)
                    .header(HttpHeaders.AUTHORIZATION, token))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return 'Not Found' when activity that should be deleted does not exist")
    void deleteActivityNotFound() {
        when(positionService.get(1)).thenReturn(Optional.empty());

        try {
            mockMvc.perform(MockMvcRequestBuilders.delete(POSITIONS_URI_ID, 1)
                    .header(HttpHeaders.AUTHORIZATION, token))
                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Applicants with Position Id")
    void testGetApplicantsByPositionIdOk() {
        when(positionService.get(1)).thenReturn(Optional.of(position));

        try {
            mockMvc.perform(MockMvcRequestBuilders.get(POSITIONS_URI_ID + "/applicants", 1)
                    .header(HttpHeaders.AUTHORIZATION, token))

                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when retrieving all Applicants with non existing Position Id")
    void testGetApplicantsByPositionIdNotFound() {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get(POSITIONS_URI_ID + "/applicants", 1)
                    .header(HttpHeaders.AUTHORIZATION, token))

                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}