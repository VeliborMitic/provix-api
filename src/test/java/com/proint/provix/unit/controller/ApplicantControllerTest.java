package com.proint.provix.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.model.applicant.ApplicantPostModel;
import com.proint.provix.security.UserCredentials;
import com.proint.provix.service.impl.ApplicantServiceImpl;
import com.proint.provix.service.impl.PositionServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class ApplicantControllerTest {

    @MockBean
    private ApplicantServiceImpl applicantServiceMock;

    @MockBean
    private PositionServiceImpl positionServiceMock;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MockMvc mockMvc;

    private String token;

    private ApplicantPostModel applicantPostModel;

    private Applicant applicant;

    private Position position;

    private static final Logger logger = LoggerFactory.getLogger(ApplicantControllerTest.class);
    private static final String BASE_URI = "/api/v1/applicants";
    private static final String SLASH_ID = "/{id}";
    private static final String NAME = "John Smith";
    private static final String EMAIL = "john.smith@prime.com";
    private static final String PHONE = "0652365600";
    private static final String EDUCATION = "Machine Engineering";
    private static final Integer POSITION_ID = 1;


    @BeforeEach
    void initData() {
        position = new Position();
        position.setId(1);
        position.setName("Project Manager");
        position.setDescription("Project management");
        position.setStartDate(Instant.parse("2019-05-10T12:30:00.000Z"));
        position.setEndDate(Instant.parse("2019-06-10T12:30:00.000Z"));
        doReturn(Optional.of(position)).when(positionServiceMock).get(1);

        applicantPostModel = new ApplicantPostModel();
        applicantPostModel.setName(NAME);
        applicantPostModel.setEmail(EMAIL);
        applicantPostModel.setPhone(PHONE);
        applicantPostModel.setEducation(EDUCATION);
        applicantPostModel.setPositionId(POSITION_ID);

        applicant = new Applicant();
        applicant.setId(1);
        applicant.setName(NAME);
        applicant.setEmail(EMAIL);
        applicant.setPhone(PHONE);
        applicant.setEducation(EDUCATION);
        applicant.setDateTime(Instant.now());
        applicant.setPosition(position);
    }

    @Test
    @DisplayName("Return '200 OK' when Applicant exists")
    void testGetApplicantOk() {
        doReturn(Optional.of(applicant)).when(applicantServiceMock).get(1);

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("id").value(1));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Applicant does not exist")
    void testGetApplicantNotFound() {
        doReturn(Optional.empty()).when(applicantServiceMock).get(1);

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testGetApplicantBadRequest() {
        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, "a")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Applicants")
    void testGetApplicantsOk() {
        try {
            mockMvc.perform(get(BASE_URI)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    @Test
    @DisplayName("Return '201 Created' when Applicant is created")
    void testCreateApplicantCreated() {

        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);
        mapped.setId(1);
        mapped.setPosition(position);

        doReturn(Optional.of(mapped)).when(applicantServiceMock).create(any());

        try {
            mockMvc.perform(post(BASE_URI)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(applicantPostModel)))

                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("id").value(1));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '409 Conflict' on attempting to create an Applicant with existing email")
    void testCreateApplicantConflict() {
        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);

        doReturn(Optional.of(mapped)).when(applicantServiceMock).getByEmail(applicantPostModel.getEmail());

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(applicantPostModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))

                    .andExpect(status().isConflict());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @DisplayName("Return '400 Bad Request' when create request does not contain valid body")
    void testCreateApplicantBadRequest(ApplicantPostModel applicantPostModel) {
        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);

        doReturn(Optional.of(mapped)).when(applicantServiceMock).create(mapped);

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(applicantPostModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when Applicant is updated successfully")
    void testUpdateApplicantOk() {
        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);
        mapped.setId(1);

        doReturn(Optional.of(mapped)).when(applicantServiceMock).get(1);
        doReturn(Optional.of(mapped)).when(applicantServiceMock).update(any());

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(applicantPostModel)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Applicant to be updated does not exist")
    void testUpdateApplicantNotFound() {
        doReturn(Optional.empty()).when(applicantServiceMock).get(1);

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(applicantPostModel)))

                    .andExpect(status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testUpdateApplicantBadRequest() {
        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);
        mapped.setId(1);

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, "a")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '204 No Content' when applicant is successfully deleted")
    void testDeleteApplicantNoContent() {
        doReturn(Optional.of(applicant)).when(applicantServiceMock).get(1);

        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isNoContent());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when applicant that should be deleted does not exist")
    void testDeleteApplicantNotFound() {
        doReturn(Optional.empty()).when(applicantServiceMock).get(1);
        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, 1)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isNotFound());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid.")
    void testDeleteApplicantBadRequest() {
        doReturn(Optional.empty()).when(applicantServiceMock).get(1);
        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, "a")
                    .header(AUTHORIZATION, token))
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> dataProvider() {
        ApplicantPostModel applicantWithoutName = new ApplicantPostModel();
        applicantWithoutName.setEmail(EMAIL);
        applicantWithoutName.setPhone(PHONE);
        applicantWithoutName.setEducation(EDUCATION);
        applicantWithoutName.setPositionId(POSITION_ID);

        ApplicantPostModel applicantWithoutEmail = new ApplicantPostModel();
        applicantWithoutEmail.setName(NAME);
        applicantWithoutEmail.setPhone(PHONE);
        applicantWithoutEmail.setEducation(EDUCATION);
        applicantWithoutEmail.setPositionId(POSITION_ID);

        ApplicantPostModel applicantWithoutPhone = new ApplicantPostModel();
        applicantWithoutPhone.setName(NAME);
        applicantWithoutPhone.setEmail(EMAIL);
        applicantWithoutPhone.setEducation(EDUCATION);
        applicantWithoutPhone.setPositionId(POSITION_ID);

        ApplicantPostModel applicantWithoutEducation = new ApplicantPostModel();
        applicantWithoutEducation.setName(NAME);
        applicantWithoutEducation.setEmail(EMAIL);
        applicantWithoutEducation.setPhone(PHONE);
        applicantWithoutEducation.setPositionId(POSITION_ID);

        ApplicantPostModel applicantWithoutPositioniD = new ApplicantPostModel();
        applicantWithoutPositioniD.setName(NAME);
        applicantWithoutPositioniD.setEmail(EMAIL);
        applicantWithoutPositioniD.setPhone(PHONE);
        applicantWithoutPositioniD.setEducation(EDUCATION);

        return Stream.of(
                Arguments.of(applicantWithoutName),
                Arguments.of(applicantWithoutEmail),
                Arguments.of(applicantWithoutPhone),
                Arguments.of(applicantWithoutEducation),
                Arguments.of(applicantWithoutPositioniD)
        );
    }

    @BeforeAll
    void authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@provix.com");
        userCredentials.setPassword("Qwerty123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(post("/api/v1/auth")
                    .contentType(APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        token = Objects.requireNonNull(mvcResult).getResponse().getHeader(AUTHORIZATION);
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}