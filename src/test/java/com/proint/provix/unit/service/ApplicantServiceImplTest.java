package com.proint.provix.unit.service;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.repository.ApplicantRepository;
import com.proint.provix.service.impl.ApplicantServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.springframework.data.domain.Sort;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class ApplicantServiceImplTest {

    @Mock
    private ApplicantRepository mockApplicantRepository;

    private ApplicantServiceImpl applicantServiceImplUnderTest;

    private Applicant applicant;

    private static final String EMAIL = "john.doe@prime.com";

    ApplicantServiceImplTest() {
        applicant = new Applicant();
        applicant.setId(1);
        applicant.setName("John Doe");
        applicant.setEmail(EMAIL);
        applicant.setPhone("381641112223");
        applicant.setEducation("Machine Engineering");
        applicant.setDateTime(Instant.parse("2019-05-10T12:30:00.000Z"));
        Position position1 = new Position();
        position1.setId(1);
        applicant.setPosition(position1);
    }

    @BeforeEach
    void setUp() {
        initMocks(this);
        applicantServiceImplUnderTest = new ApplicantServiceImpl(mockApplicantRepository);

        doReturn(Optional.of(applicant)).when(mockApplicantRepository).findById(1);
        doReturn(Optional.of(applicant)).when(mockApplicantRepository).findByEmail(EMAIL);
    }

    @Test
    void testGet() {
        final Optional<Applicant> result = applicantServiceImplUnderTest.get(1);

        result.ifPresent(a -> {
            assertEquals(a, result.get());
            assertEquals(a.getName(), result.get().getName());
            assertEquals(a.getClass(), result.get().getClass());
        });
    }

    @Test
    void testGetByEmail() {
        final Optional<Applicant> result = applicantServiceImplUnderTest.getByEmail(EMAIL);

        result.ifPresent(a -> {
            assertEquals(a.getEmail(), result.get().getEmail());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testGetAll() {
        final List<Applicant> applicants = new ArrayList<>();
        Applicant applicant2 = new Applicant();
        applicant2.setId(2);
        applicants.add(applicant2);
        applicants.add(applicant);
        doReturn(applicants).when(mockApplicantRepository).findAll();

        final List<Applicant> result = applicantServiceImplUnderTest.get();

        assertFalse(result.isEmpty());
        assertEquals(applicants.get(0), result.get(0));
        assertEquals(2, result.size());
        assertEquals(applicants.getClass(), result.getClass());
        assertEquals(applicants, result);
    }

    @Test
    void testCreate() {
        doReturn(applicant).when(mockApplicantRepository).save(applicant);

        final Optional<Applicant> result = applicantServiceImplUnderTest.create(applicant);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testUpdate() {
        Applicant updated = new Applicant();
        updated.setId(1);
        doReturn(updated).when(mockApplicantRepository).save(applicant);

        final Optional<Applicant> result = applicantServiceImplUnderTest.update(applicant);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testDelete() {
        applicantServiceImplUnderTest.delete(1);

        verify(mockApplicantRepository).deleteById(1);
    }

    @Test
    void testGetThrowsBadRequestOnBadApplicantStatus() {
        Sort sort = null;
        String search = "applicantStatus~asd";

        assertThrows(ResponseStatusException.class, () -> applicantServiceImplUnderTest.get(search, sort));
    }
}