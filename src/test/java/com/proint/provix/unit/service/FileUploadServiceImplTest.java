package com.proint.provix.unit.service;

import com.proint.provix.configuration.FileUploadConfiguration;
import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.repository.FileUploadRepository;
import com.proint.provix.service.impl.ApplicantServiceImpl;
import com.proint.provix.service.impl.FileUploadServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class FileUploadServiceImplTest {

    @Mock
    private FileUploadRepository mockFileUploadRepository;
    @Mock
    private ApplicantServiceImpl mockApplicantService;
    @Mock
    private FileUploadConfiguration mockConfiguration;

    private FileUploadServiceImpl fileUploadServiceImplUnderTest;

    private Applicant applicant;
    private MockMultipartFile mockMultipartFile;

    FileUploadServiceImplTest() {
        applicant = new Applicant();
        applicant.setId(1);
        applicant.setName("Zulema Dugdale");
        applicant.setEmail("zulemadugdale@example.com");
        applicant.setPhone("+372 250 140 3270");
        applicant.setEducation("Bachelor");
        applicant.setDateTime(Instant.parse("2019-05-10T12:30:00.000Z"));
        Position position1 = new Position();
        position1.setId(26);
        applicant.setPosition(position1);

        mockMultipartFile = new MockMultipartFile
                ("files", "regular.pdf", "application/pdf", "first pdf".getBytes());
    }

    @BeforeEach
    void setUp() {
        initMocks(this);
        fileUploadServiceImplUnderTest = new FileUploadServiceImpl(
                mockFileUploadRepository,
                mockApplicantService,
                mockConfiguration);

        doReturn(Optional.of(applicant)).when(mockApplicantService).get(1);
    }

    @Test
    void testStoreFileOk() {
        final MultipartFile file = mockMultipartFile;

        doReturn(Optional.of(applicant)).when(mockApplicantService).update(applicant);

        final Map<Boolean, String> uploadResult= fileUploadServiceImplUnderTest.storeFile(applicant, file);

        assertTrue(uploadResult.containsKey(true));
    }

    @Test
    void testDeleteUploadedFiles() {
        final Integer applicantId = 1;

        final boolean result = fileUploadServiceImplUnderTest.deleteUploadedFiles(applicantId);

        assertTrue(result);
    }
}
