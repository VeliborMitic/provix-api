package com.proint.provix.unit.service;

import com.proint.provix.entity.Position;
import com.proint.provix.repository.PositionRepository;
import com.proint.provix.service.impl.PositionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class PositionServiceImplTest {

    @Mock
    private PositionRepository positionRepository;

    @InjectMocks
    private PositionServiceImpl positionService;

    private static final Integer POSITION_ID = 1;
    private static final String NAME = "Banker";
    private Position position;

    @BeforeEach
    void init() {

        position = new Position();
        position.setId(POSITION_ID);
        position.setName(NAME);
        position.setDescription("test");
        position.setStartDate(Instant.parse("2019-05-08T12:47:27.985Z"));
        position.setEndDate(Instant.parse("2019-06-08T12:47:27.985Z"));
    }

    @Test
    void testGetById() {
        when(positionRepository.findById(POSITION_ID)).thenReturn(Optional.ofNullable(position));
        Optional<Position> actualPosition = positionService.get(POSITION_ID);
        assertEquals(actualPosition, Optional.of(position));
    }

    @Test
    void testGetByName() {
        when(positionRepository.findByNameIs(NAME)).thenReturn(Optional.ofNullable(position));
        Optional<Position> actualPosition = positionService.getByName(NAME);
        assertEquals(actualPosition, Optional.of(position));
    }

    @Test
    void testGetAll() {
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        when(positionRepository.findAll()).thenReturn(positions);
        positionService.get();
        assertEquals(position, positions.get(0));
    }

    @Test
    void testCreate() {
        when(positionRepository.save(position)).thenReturn(position);
        Optional<Position> actualPosition = positionService.create(position);
        assertEquals(actualPosition, Optional.of(position));
    }

    @Test
    void testDelete() {
        doNothing().when(positionRepository).deleteById(POSITION_ID);
        positionService.delete(POSITION_ID);
        verify(positionRepository).deleteById(POSITION_ID);
    }

    @Test
    void testGetThrowsBadRequestOnBadSearchTerm() {
        Sort sort = null;
        String search = "nam~er";

        assertThrows(ResponseStatusException.class, () -> positionService.get(search, sort));
    }

    @Test
    void testGetThrowsBadRequestOnBadDateTime() {
        Sort sort = null;
        String search = "startDate~2018-02-02";

        assertThrows(ResponseStatusException.class, () -> positionService.get(search, sort));
    }

    @Test
    void testGetThrowsBadRequestOnBadOperator() {
        Sort sort = null;
        String search = "name!er";

        assertThrows(ResponseStatusException.class, () -> positionService.get(search, sort));
    }
}