package com.proint.provix.integration.controller;

import org.junit.jupiter.params.provider.Arguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

class IntegrationTestUtils {

    private static final String DOMAIN = "http://localhost:";
    private static final String API_VER = "/api/v1/";
    private static final Logger logger = LoggerFactory.getLogger(IntegrationTestUtils.class);

    private IntegrationTestUtils() {
    }

    static String buildURL(Integer port, String resource) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(DOMAIN).append(port).append(API_VER).append(resource);

        try {
            URL url = new URL(stringBuilder.toString());
            return url.toString();
        } catch (MalformedURLException e) {
            logger.error(e.getMessage(), e);
            return "";
        }
    }

    static String buildURL(Integer port, String resource, Map<String, String> urlVariables) {
        StringBuilder builder = new StringBuilder();
        builder.append(DOMAIN).append(port).append(API_VER).append(resource).append("?");
        urlVariables.forEach((key, value) ->
                builder.append(key).append("=").append(value).append("&"));

        try {
            URL url = new URL(removeLastCharRegexOptional(builder.toString()));
            return url.toString();
        } catch (MalformedURLException e) {
            logger.error(e.getMessage(), e);
            return "";
        }
    }

    private static String removeLastCharRegexOptional(String s) {
        return Optional.ofNullable(s)
                .map(str -> str.replaceAll(".$", ""))
                .orElse(s);
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> dataProvider() {
        Map<String, String> urlWithoutVariables = new HashMap<>();

        Map<String, String> urlVariablesOnlyPage = new HashMap<>();
        urlVariablesOnlyPage.put("page", "0");
        urlVariablesOnlyPage.put("size", "10");

        Map<String, String> urlVariablesOnlySort = new HashMap<>();
        urlVariablesOnlySort.put("sort", "id,ASC");

        Map<String, String> urlVariablesPagingAndSorting = new HashMap<>();
        urlVariablesPagingAndSorting.put("page", "0");
        urlVariablesPagingAndSorting.put("size", "15");
        urlVariablesPagingAndSorting.put("sort", "id,ASC");

        return Stream.of(
                Arguments.of(urlWithoutVariables),
                Arguments.of(urlVariablesOnlyPage),
                Arguments.of(urlVariablesOnlySort),
                Arguments.of(urlVariablesPagingAndSorting)
        );
    }

    static Stream<Arguments> dataProviderSearchPositions() {
        Map<String, String> urlVariablesWithSearch = new HashMap<>();
        urlVariablesWithSearch.put("search", "name~ian,id>2,id<90,startDate>2017-01-01T11:11:11Z,endDate<2020-01-01T11:11:11Z");
        urlVariablesWithSearch.put("page", "0");
        urlVariablesWithSearch.put("size", "15");
        urlVariablesWithSearch.put("sort", "name,ASC");

        return Stream.of(
                Arguments.of(urlVariablesWithSearch)
        );
    }
}
