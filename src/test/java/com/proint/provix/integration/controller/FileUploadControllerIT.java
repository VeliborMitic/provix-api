package com.proint.provix.integration.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.MalformedURLException;
import java.net.URL;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class FileUploadControllerIT {

    @LocalServerPort
    private Integer port;

    @Autowired
    private MockMvc mockMvc;

    private MockMultipartFile validMultipartFile1Mock;
    private MockMultipartFile validMultipartFile2Mock;
    private MockMultipartFile validMultipartFile3Mock;
    private MockMultipartFile validMultipartFile4Mock;
    private MockMultipartFile validMultipartFile5Mock;
    private MockMultipartFile validMultipartFile6Mock;
    private MockMultipartFile invalidMimeTypeFileMock;

    private MockHttpServletRequestBuilder builder;

    private static final String APPLICANTS_ONE = "applicants/1";
    private static final String FILES = "files";
    private static final Logger logger = LoggerFactory.getLogger(FileUploadControllerIT.class);

    private static URL buildURL(Integer port) throws MalformedURLException {
        return new URL("http://localhost:" + port + "/api/v1/" + APPLICANTS_ONE + "/files");
    }

    @BeforeEach
    void init() {
        validMultipartFile1Mock = new MockMultipartFile
                (FILES, "first.pdf", "application/pdf", "first pdf".getBytes());
        validMultipartFile2Mock = new MockMultipartFile
                (FILES, "second.jpeg", "image/jpeg", "second jpeg".getBytes());
        validMultipartFile3Mock = new MockMultipartFile
                (FILES, "third.doc", "application/msword", "third doc".getBytes());
        validMultipartFile4Mock = new MockMultipartFile
                (FILES, "fourth.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "fourth docx".getBytes());
        validMultipartFile5Mock = new MockMultipartFile
                (FILES, "fifth.png", "image/png", "fifth doc".getBytes());
        validMultipartFile6Mock = new MockMultipartFile
                (FILES, "sixth.zip", "application/zip", "sixth doc".getBytes());
        invalidMimeTypeFileMock = new MockMultipartFile
                (FILES, "invalid.txt", "text/plain", "invalid txt".getBytes());

    }

    @Test
    @DisplayName("Integration Test - Returns '200 OK' when uploading valid files array")
    void testUploadValidFiles() {

        try {
            builder = MockMvcRequestBuilders.multipart(buildURL(port).toString())
                    .file(validMultipartFile1Mock)
                    .file(validMultipartFile2Mock)
                    .file(validMultipartFile3Mock)
                    .file(validMultipartFile4Mock)
                    .file(validMultipartFile5Mock);

            mockMvc.perform(builder)
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Integration Test - Returns '400 Bad Request' when uploading to many files")
    void testUploadToMannyFiles() {

        try {
            builder = MockMvcRequestBuilders.multipart(buildURL(port).toString())
                    .file(validMultipartFile1Mock)
                    .file(validMultipartFile2Mock)
                    .file(validMultipartFile3Mock)
                    .file(validMultipartFile4Mock)
                    .file(validMultipartFile5Mock)
                    .file(validMultipartFile6Mock);

            mockMvc.perform(builder)
                    .andExpect(MockMvcResultMatchers.status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Integration Test - Returns '400 Bad Request' when uploading file with invalid mime type")
    void testUploadInvalidFiles() {

        try {
            builder = MockMvcRequestBuilders.multipart(buildURL(port).toString())
                    .file(validMultipartFile1Mock)
                    .file(validMultipartFile2Mock)
                    .file(invalidMimeTypeFileMock);

            mockMvc.perform(builder)
                    .andExpect(MockMvcResultMatchers.status().isBadRequest());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}

