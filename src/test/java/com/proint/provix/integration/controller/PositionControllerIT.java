package com.proint.provix.integration.controller;

import com.proint.provix.model.applicant.ApplicantGetModel;
import com.proint.provix.model.position.PositionGetModel;
import com.proint.provix.model.position.PositionPostModel;
import com.proint.provix.security.UserCredentials;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.proint.provix.integration.controller.IntegrationTestUtils.buildURL;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SuppressWarnings("all")
class PositionControllerIT {

    private static final Integer ID_TO_TEST = 1;
    private static final String NAME_TO_TEST = "Electrical Engineer";
    private static final String DESCRIPTION_TO_TEST = "Morbi vel lectus in quam " +
            "fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, " +
            "cursus id, turpis. Integer aliquet, massa id lobortis convallis, " +
            "tortor risus dapibus augue, vel accumsan tellus nisi eu orci. " +
            "Mauris lacinia sapien quis libero.";
    private static final String START_DATE_TO_TEST = "2019-03-03T12:08:47Z";
    private static final String END_DATE_TO_TEST = "2019-03-06T18:14:09Z";
    private static final String NAME_TO_TEST_THREE = "Plumber";
    private static final String START_DATE_TO_TEST_THREE = "2019-05-08T09:50:27Z";
    private static final String POSITIONS_ONE = "positions/1";
    private static final Logger logger = LoggerFactory.getLogger(PositionControllerIT.class);

    @LocalServerPort
    private Integer port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private String authenticate() throws Exception {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@provix.com");
        userCredentials.setPassword("Qwerty123456");

        HttpEntity<UserCredentials> httpEntity = new HttpEntity<>(userCredentials);

        ResponseEntity<String> responseEntity = null;
        responseEntity = testRestTemplate
                .exchange(buildURL(port, "auth").toString(),
                        HttpMethod.POST,
                        httpEntity,
                        String.class);

        return responseEntity.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
    }

    @Test
    @DisplayName("Get by ID IT")
    void getPositionById() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<PositionGetModel> response = null;
        response = testRestTemplate
                .exchange(buildURL(port, POSITIONS_ONE).toString(),
                        HttpMethod.GET,
                        httpEntity,
                        PositionGetModel.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(ID_TO_TEST, Objects.requireNonNull(response.getBody()).getId());
        assertEquals(NAME_TO_TEST, response.getBody().getName());
        assertEquals(DESCRIPTION_TO_TEST, response.getBody().getDescription());
        assertEquals(START_DATE_TO_TEST, response.getBody().getStartDate());
        assertEquals(END_DATE_TO_TEST, response.getBody().getEndDate());
    }

    @ParameterizedTest
    @MethodSource("com.proint.provix.integration.controller.IntegrationTestUtils#dataProvider")
    @DisplayName("Integration Test - Get all Applicants by Position Id")
    void getAllApplicantsByPositionId(Map<String, String> urlVariables) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<PagedResources<ApplicantGetModel>> responseEntity = testRestTemplate
                .exchange(buildURL(port, POSITIONS_ONE + "/applicants", urlVariables).toString(),
                        HttpMethod.GET,
                        httpEntity,
                        new ParameterizedTypeReference<PagedResources<ApplicantGetModel>>() {
                        });
        PagedResources<ApplicantGetModel> applicantResources = responseEntity.getBody();
        Collection<ApplicantGetModel> page = applicantResources.getContent();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ApplicantGetModel.class, page.stream().findFirst().get().getClass());
    }

    @Test
    @DisplayName("Create IT")
    void createPosition() throws Exception {
        PositionPostModel positionPostModel = new PositionPostModel();
        positionPostModel.setName("Mechanic");
        positionPostModel.setDescription(DESCRIPTION_TO_TEST);
        positionPostModel.setStartDate("2019-05-08T12:47:27Z");
        positionPostModel.setEndDate("2019-06-08T12:47:27Z");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity<PositionPostModel> httpEntity = new HttpEntity<>(positionPostModel, httpHeaders);

        ResponseEntity<PositionGetModel> response = null;
        response = testRestTemplate
                .exchange(buildURL(port, "positions").toString(),
                        HttpMethod.POST,
                        httpEntity,
                        PositionGetModel.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(positionPostModel.getName(), Objects.requireNonNull(response.getBody()).getName());
        assertEquals(positionPostModel.getStartDate(), response.getBody().getStartDate());
    }

    @Test
    @DisplayName("Update IT")
    void updatePosition() throws Exception {
        PositionPostModel positionPostModel = new PositionPostModel();
        positionPostModel.setName(NAME_TO_TEST_THREE);
        positionPostModel.setDescription(DESCRIPTION_TO_TEST);
        positionPostModel.setStartDate(START_DATE_TO_TEST_THREE);
        positionPostModel.setEndDate("2019-06-08T12:47:27.985Z");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity<PositionPostModel> httpEntity = new HttpEntity<>(positionPostModel, httpHeaders);

        ResponseEntity<PositionGetModel> response = null;
        response = testRestTemplate
                .exchange(buildURL(port, "positions/60").toString(),
                        HttpMethod.PUT,
                        httpEntity,
                        PositionGetModel.class);

        assertEquals(positionPostModel.getName(), Objects.requireNonNull(response.getBody()).getName());
        assertEquals(START_DATE_TO_TEST_THREE, response.getBody().getStartDate());
    }

    @Test
    @DisplayName("Delete IT")
    void testDeleteApplicant() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity<PositionPostModel> httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<PositionGetModel> response = null;
        response = testRestTemplate
                .exchange(buildURL(port, "positions/50").toString(),
                        HttpMethod.DELETE,
                        httpEntity,
                        PositionGetModel.class);

        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    @DisplayName("Integration Test - Get - with Search ")
    void testGetWithSearch() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        Map<String, String> urlVariablesWithSearch = new HashMap<>();
        urlVariablesWithSearch.put("search", "name~ian,id>2,id<90,startDate>2017-01-01T11:11:11Z,endDate<2020-01-01T11:11:11Z");
        urlVariablesWithSearch.put("page", "0");
        urlVariablesWithSearch.put("size", "15");

        ResponseEntity<PagedResources<PositionGetModel>> responseEntity = testRestTemplate
                .exchange(buildURL(port, "positions", urlVariablesWithSearch).toString(),
                        HttpMethod.GET,
                        httpEntity,
                        new ParameterizedTypeReference<PagedResources<PositionGetModel>>() {
                        });
        PagedResources<PositionGetModel> positionResources = responseEntity.getBody();
        Collection<PositionGetModel> page = positionResources.getContent();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(PositionGetModel.class, page.stream().findFirst().get().getClass());
    }
}