package com.proint.provix.integration.controller;

import com.proint.provix.model.applicant.ApplicantGetModel;
import com.proint.provix.model.applicant.ApplicantPostModel;
import com.proint.provix.model.applicant.ApplicantPutModel;
import com.proint.provix.security.UserCredentials;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.proint.provix.integration.controller.IntegrationTestUtils.buildURL;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@SuppressWarnings("all")
class ApplicantControllerIT {
    @LocalServerPort
    private Integer port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static final Integer ID_TO_TEST = 1;
    private static final String NAME_TO_TEST = "Zulema Dugdale";
    private static final String EMAIL_TO_TEST = "zulemadugdale@example.com";
    private static final String PHONE_TO_TEST = "+372 250 140 3270";
    private static final String STATUS_TO_TEST = "ACCEPTED";
    private static final String DATE_TIME_TO_TEST = "2018-12-15T23:48:36Z";
    private static final Integer POSITION_ID_TO_TEST = 26;
    private static final String APPLICANTS_ONE = "applicants/1";
    private static final Logger logger = LoggerFactory.getLogger(ApplicantControllerIT.class);

    private ApplicantPostModel applicantPostModel;
    private ApplicantPutModel applicantPutModel;

    @BeforeEach
    void init() {
        applicantPostModel = new ApplicantPostModel();
        applicantPostModel.setName("John Smith");
        applicantPostModel.setEmail("john.smith@prime.com");
        applicantPostModel.setPhone("+372 250 140 3270");
        applicantPostModel.setEducation("Machine Engineering");
        applicantPostModel.setPositionId(26);

        applicantPutModel = new ApplicantPutModel();
        applicantPutModel.setName("Zulema Dugdale");
        applicantPutModel.setEmail("zulemadugdale@example.com");
        applicantPutModel.setPhone("381641112223");
        applicantPutModel.setEducation("Bachelor");
        applicantPutModel.setPositionId(26);
        applicantPutModel.setApplicantStatus("ACCEPTED");
    }

    private String authenticate() throws Exception {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@provix.com");
        userCredentials.setPassword("Qwerty123456");

        HttpEntity<UserCredentials> httpEntity = new HttpEntity<>(userCredentials);

        ResponseEntity<String> responseEntity = testRestTemplate
                .exchange(buildURL(port, "auth").toString(),
                        HttpMethod.POST,
                        httpEntity,
                        String.class);

        return Objects.requireNonNull(responseEntity.getHeaders().get(HttpHeaders.AUTHORIZATION)).get(0);
    }

    @Test
    @DisplayName("Integration Test - Get Applicant by ID")
    void testGetApplicantById() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<ApplicantGetModel> responseEntity = testRestTemplate
                .exchange(buildURL(port, APPLICANTS_ONE).toString(),
                        HttpMethod.GET,
                        httpEntity,
                        ApplicantGetModel.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ApplicantGetModel.class, Objects.requireNonNull(responseEntity.getBody()).getClass());
        assertEquals(ID_TO_TEST, Objects.requireNonNull(responseEntity.getBody()).getId());
        assertEquals(NAME_TO_TEST, responseEntity.getBody().getName());
        assertEquals(EMAIL_TO_TEST, responseEntity.getBody().getEmail());
        assertEquals("+372 250 140 3270", responseEntity.getBody().getPhone());
        assertEquals(STATUS_TO_TEST, responseEntity.getBody().getApplicantStatus());
        assertEquals(DATE_TIME_TO_TEST, responseEntity.getBody().getDateTime());
        assertEquals(POSITION_ID_TO_TEST, responseEntity.getBody().getPositionId());
    }

    @ParameterizedTest
    @MethodSource("com.proint.provix.integration.controller.IntegrationTestUtils#dataProvider")
    @DisplayName("Integration Test - - ParameterizedTest - Get")
    void testGetAllApplicants(Map<String, String> urlVariables) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<PagedResources<ApplicantGetModel>> responseEntity = null;
        try {
            responseEntity = testRestTemplate
                    .exchange(buildURL(port, "applicants", urlVariables).toString(),
                            HttpMethod.GET,
                            httpEntity,
                            new ParameterizedTypeReference<PagedResources<ApplicantGetModel>>() {
                            });
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        PagedResources<ApplicantGetModel> applicantResources = responseEntity.getBody();
        Collection<ApplicantGetModel> page = applicantResources.getContent();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ApplicantGetModel.class, page.stream().findFirst().get().getClass());
    }

    @Test
    @DisplayName("Integration Test - Create Applicant")
    void testCreateApplicant() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity<ApplicantPostModel> httpEntity = new HttpEntity<>(applicantPostModel, httpHeaders);

        ResponseEntity<ApplicantGetModel> responseEntity = testRestTemplate
                .exchange(buildURL(port, "applicants").toString(),
                        HttpMethod.POST,
                        httpEntity,
                        ApplicantGetModel.class);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(applicantPostModel.getName(), Objects.requireNonNull(responseEntity.getBody()).getName());
        assertEquals(POSITION_ID_TO_TEST, Objects.requireNonNull(responseEntity.getBody()).getPositionId());
    }

    @Test
    @DisplayName("Integration Test - Update Applicant")
    void testUpdateApplicant() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity<ApplicantPutModel> httpEntity = new HttpEntity<>(applicantPutModel, httpHeaders);

        ResponseEntity<ApplicantGetModel> responseEntity = testRestTemplate.
                exchange(buildURL(port, APPLICANTS_ONE).toString(),
                        HttpMethod.PUT,
                        httpEntity,
                        ApplicantGetModel.class);

        assertEquals(applicantPutModel.getName(), Objects.requireNonNull(responseEntity.getBody()).getName());
        assertEquals(applicantPutModel.getEmail(), Objects.requireNonNull(responseEntity.getBody()).getEmail());
    }

    @Test
    @DisplayName("Integration Test - Delete Applicant")
    void testDeleteApplicant() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<ApplicantGetModel> responseEntity = testRestTemplate
                .exchange(buildURL(port, "applicants/55").toString(),
                        HttpMethod.DELETE,
                        httpEntity,
                        ApplicantGetModel.class);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    @DisplayName("Integration Test - Get with Search")
    void testGetWithSearch() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticate());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);

        Map<String, String> urlVariablesWithSearch = new HashMap<>();
        urlVariablesWithSearch.put("search", "name~ian,id>2,id<900,dateTime>2017-01-01T11:11:11Z,dateTime<2020-01-01T11:11:11Z");
        urlVariablesWithSearch.put("page", "0");
        urlVariablesWithSearch.put("size", "15");

        ResponseEntity<PagedResources<ApplicantGetModel>> responseEntity = null;
        try {
            responseEntity = testRestTemplate
                    .exchange(buildURL(port, "applicants", urlVariablesWithSearch).toString(),
                            HttpMethod.GET,
                            httpEntity,
                            new ParameterizedTypeReference<PagedResources<ApplicantGetModel>>() {
                            });
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        PagedResources<ApplicantGetModel> applicantResources = responseEntity.getBody();
        Collection<ApplicantGetModel> page = applicantResources.getContent();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ApplicantGetModel.class, page.stream().findFirst().get().getClass());
    }
}