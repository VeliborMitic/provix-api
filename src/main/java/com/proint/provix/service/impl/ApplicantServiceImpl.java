package com.proint.provix.service.impl;

import com.proint.provix.entity.Applicant;
import com.proint.provix.repository.ApplicantRepository;
import com.proint.provix.service.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ApplicantServiceImpl extends BaseServiceImpl<Applicant> implements ApplicantService {

    private ApplicantRepository applicantRepository;

    @Autowired
    public ApplicantServiceImpl(ApplicantRepository applicantRepository) {
        super(applicantRepository);
        this.applicantRepository = applicantRepository;
    }

    @Override
    public Optional<Applicant> getByEmail(String email) {
        return applicantRepository.findByEmail(email);
    }

    @Override
    public Optional<Page<Applicant>> getAllByPositionId(Integer id, Pageable pageable) {
        return Optional.of(applicantRepository.findAllByPositionId(id, pageable));
    }
}