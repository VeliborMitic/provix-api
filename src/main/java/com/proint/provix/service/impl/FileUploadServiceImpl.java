package com.proint.provix.service.impl;

import com.proint.provix.configuration.FileUploadConfiguration;
import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.FileUpload;
import com.proint.provix.repository.FileUploadRepository;
import com.proint.provix.service.ApplicantService;
import com.proint.provix.service.FileUploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class FileUploadServiceImpl extends BaseServiceImpl<FileUpload> implements FileUploadService {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadServiceImpl.class);
    private FileUploadRepository fileUploadRepository;
    private ApplicantService applicantService;
    private final String rootPath;
    private Map<Boolean, String> uploadResultsMap;

    @Autowired
    public FileUploadServiceImpl(FileUploadRepository fileUploadRepository,
                                 ApplicantServiceImpl applicantService,
                                 FileUploadConfiguration configuration) {
        super(fileUploadRepository);
        this.fileUploadRepository = fileUploadRepository;
        this.applicantService = applicantService;
        this.rootPath = configuration.getRootFolder();
        this.uploadResultsMap = new HashMap<>();
    }

    @Override
    public List<FileUpload> getAllByApplicantId(Integer id) {
        return fileUploadRepository.findAllByApplicantId(id);
    }

    public Map<Boolean, String> storeFile(Applicant applicant, MultipartFile file) {

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String uuidDirectory = getUploadFolder(applicant);
        Path applicantDirectory = Paths.get(rootPath, uuidDirectory).normalize();
        Path fileToUploadPath = Paths.get(rootPath, uuidDirectory, fileName).normalize();

        if (fileUploadRepository.findAllByApplicantId(applicant.getId()).size() == 5 && !fileToUploadPath.toFile().exists()) {
            uploadResultsMap.put(false, "Maximum number of files per user is 5!");
            return uploadResultsMap;
        }

        try {
            if (fileToUploadPath.toFile().exists()) {
                Files.delete(fileToUploadPath);
            }
            Files.createDirectories(applicantDirectory);
            Files.copy(file.getInputStream(), fileToUploadPath);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            uploadResultsMap.put(false, "Something went wrong. Unsuccessful upload " + fileName);
            return uploadResultsMap;
        }

        try {
            FileUpload fileUpload = new FileUpload();
            fileUpload.setFileName(fileName);
            fileUpload.setDownloadPath(applicantDirectory.toString());
            fileUpload.setFileType(file.getContentType());
            fileUpload.setApplicant(applicant);
            fileUpload.setSize(file.getSize());

            Optional<FileUpload> optionalFileUpload = fileUploadRepository.findByFileName(fileName);
            optionalFileUpload.ifPresent(upload -> fileUpload.setId(upload.getId()));
            fileUploadRepository.save(fileUpload);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            uploadResultsMap.put(false, "Something went wrong. Database error uploading file " + fileName);
            return uploadResultsMap;
        }

        uploadResultsMap.put(true, null);
        return uploadResultsMap;
    }

    public boolean deleteUploadedFiles(Integer applicantId) {
        String applicantFilesDirectory;
        Optional<Applicant> optional = applicantService.get(applicantId);

        if (optional.isPresent() && optional.get().getFilesRootFolder() != null) {
            applicantFilesDirectory = optional.get().getFilesRootFolder();
            try {
                return (FileSystemUtils.deleteRecursively(Paths.get(rootPath, applicantFilesDirectory)));
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return true;
    }

    private String getUploadFolder(Applicant applicant) {
        if (applicant.getFilesRootFolder() != null) {
            return applicant.getFilesRootFolder();
        }

        String uuidFolderName = UUID.nameUUIDFromBytes(applicant.getId().toString().getBytes()).toString();
        applicant.setFilesRootFolder(uuidFolderName);
        applicantService.update(applicant);

        return uuidFolderName;
    }
}
