package com.proint.provix.service.impl;

import com.proint.provix.entity.Position;
import com.proint.provix.repository.PositionRepository;
import com.proint.provix.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PositionServiceImpl extends BaseServiceImpl<Position> implements PositionService {
    private PositionRepository positionRepository;

    @Autowired
    public PositionServiceImpl(PositionRepository positionRepository) {
        super(positionRepository);
        this.positionRepository = positionRepository;
    }

    public Optional<Position> getByName(String name) {
        return positionRepository.findByNameIs(name);
    }

    public Boolean existsByNameAndDifferentId(String name, Integer id) {
        return positionRepository.existsByNameAndDifferentId(name, id);
    }
}