package com.proint.provix.service;

import com.proint.provix.entity.Position;

import java.util.Optional;

public interface PositionService extends BaseService<Position> {

    Optional<Position> getByName(String name);

    Boolean existsByNameAndDifferentId(String name, Integer id);
}