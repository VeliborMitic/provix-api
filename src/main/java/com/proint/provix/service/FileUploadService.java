package com.proint.provix.service;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.FileUpload;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface FileUploadService extends BaseService<FileUpload> {

    List<FileUpload> getAllByApplicantId(Integer id);

    Map<Boolean, String> storeFile(Applicant applicant, MultipartFile file);
}
