package com.proint.provix.service;

import com.proint.provix.entity.Applicant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ApplicantService extends BaseService<Applicant> {

    Optional<Applicant> getByEmail(String email);

    Optional<Page<Applicant>> getAllByPositionId(Integer id, Pageable pageable);
}