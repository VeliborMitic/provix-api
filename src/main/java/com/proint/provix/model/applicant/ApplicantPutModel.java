package com.proint.provix.model.applicant;

public class ApplicantPutModel extends ApplicantModel {
    private String applicantStatus;

    public String getApplicantStatus() {
        return applicantStatus;
    }

    public void setApplicantStatus(String applicantStatus) {
        this.applicantStatus = applicantStatus;
    }
}
