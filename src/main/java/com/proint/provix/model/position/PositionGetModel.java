package com.proint.provix.model.position;

public class PositionGetModel extends PositionModel {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}