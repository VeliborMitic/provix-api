package com.proint.provix.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.Instant;

@Entity
@Table(name = "applicants")
@SuppressWarnings("all")
public class Applicant extends BaseEntity {

    @Column(nullable = false)
    @Size(max = 100)
    private String name;

    @Column(nullable = false, unique = true)
    @Email
    @Size(max = 50)
    private String email;

    @Column(nullable = false)
    @Size(max = 25)
    private String phone;

    @Column(nullable = false)
    @Size(max = 25)
    private String education;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ApplicantStatus applicantStatus = ApplicantStatus.PENDING;

    @Column(columnDefinition = "DATETIME(3) NOT NULL")
    private Instant dateTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "position_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Position position;

    @Column(unique = true)
    private String filesRootFolder;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public ApplicantStatus getApplicantStatus() {
        return applicantStatus;
    }

    public void setApplicantStatus(ApplicantStatus applicantStatus) {
        this.applicantStatus = applicantStatus;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    public void setDateTime(Instant dateTime) {
        this.dateTime = dateTime;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getFilesRootFolder() {
        return filesRootFolder;
    }

    public void setFilesRootFolder(String filesRootFolder) {
        this.filesRootFolder = filesRootFolder;
    }
}