package com.proint.provix.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "positions")
@SuppressWarnings("all")
public class Position extends BaseEntity {
    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    @Column(length = 8192)
    private String description;

    @NotNull
    private Instant startDate;

    @NotNull
    private Instant endDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return getName().equals(position.getName()) &&
                getDescription().equals(position.getDescription()) &&
                getStartDate().equals(position.getStartDate()) &&
                getEndDate().equals(position.getEndDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getStartDate(), getEndDate());
    }
}