package com.proint.provix.entity;

public enum ApplicantStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}
