package com.proint.provix.controller;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.exception.ErrorResponse;
import com.proint.provix.model.applicant.ApplicantGetModel;
import com.proint.provix.model.applicant.ApplicantPostModel;
import com.proint.provix.model.applicant.ApplicantPutModel;
import com.proint.provix.service.ApplicantService;
import com.proint.provix.service.PositionService;
import com.proint.provix.service.impl.FileUploadServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/applicants")
public class ApplicantController {

    private ApplicantService applicantService;
    private PositionService positionService;
    private FileUploadServiceImpl fileUploadService;
    private ModelMapper modelMapper;
    private static final String RESOURCE_NOT_FOUND = "Resource Not Found";
    private static final String EXISTING_MAIL = "Email already exist";
    private static final String BAD_REQUEST = "Bad Request";

    @Autowired
    public ApplicantController(ApplicantService applicantService,
                               PositionService positionService,
                               FileUploadServiceImpl fileUploadService,
                               ModelMapper modelMapper) {
        this.applicantService = applicantService;
        this.positionService = positionService;
        this.modelMapper = modelMapper;
        this.fileUploadService = fileUploadService;
    }

    @SuppressWarnings({"unchecked"})
    @GetMapping
    public HttpEntity get(
            @RequestParam(value = "search", required = false) String search, Pageable pageable) {
        Optional<Page<Applicant>> optionalPage;

        if (search != null) {
            optionalPage = applicantService.get(search, pageable);
        } else {
            optionalPage = applicantService.get(pageable);
        }

        return optionalPage.map(
                page -> {
                    List<ApplicantGetModel> applicants = page.stream()
                            .map(a -> modelMapper.map(a, ApplicantGetModel.class))
                            .collect(Collectors.toList());
                    return new ResponseEntity<>(new PageImpl(applicants, page.getPageable(),
                            page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.ok(new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Applicant> optional = applicantService.get(id);

        return optional.map(a -> ResponseEntity.ok(modelMapper.map(a, ApplicantGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public HttpEntity create(@RequestBody ApplicantPostModel applicantPostModel) {
        Applicant mapped = modelMapper.map(applicantPostModel, Applicant.class);
        Optional<Applicant> optionalByEmail = applicantService.getByEmail(mapped.getEmail());

        if (!optionalByEmail.isPresent()) {

            Optional<Position> optionalPosition = positionService.get(applicantPostModel.getPositionId());
            if (!optionalPosition.isPresent())
                return new ResponseEntity<>(new ErrorResponse(BAD_REQUEST), HttpStatus.BAD_REQUEST);
            mapped.setPosition(optionalPosition.get());

            mapped.setDateTime(Instant.now());
            Optional<Applicant> optional = applicantService.create(mapped);

            return optional.<HttpEntity>map(a -> ResponseEntity.created(URI.create("/api/v1/applicants"))
                    .body(modelMapper.map(a, ApplicantGetModel.class)))
                    .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(BAD_REQUEST), HttpStatus.BAD_REQUEST));
        }
        return new ResponseEntity<>(new ErrorResponse(EXISTING_MAIL), HttpStatus.CONFLICT);
    }

    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") Integer id, @RequestBody ApplicantPutModel applicantPutModel) {
        Optional<Applicant> optional = applicantService.get(id);
        Optional<Applicant> optionalByEmail = applicantService.getByEmail(applicantPutModel.getEmail());

        if (optionalByEmail.isPresent()
                && optional.isPresent()
                && !Objects.equals(optionalByEmail.get().getId(), id)) {
            return new ResponseEntity<>(new ErrorResponse(EXISTING_MAIL), HttpStatus.CONFLICT);
        }

        if (optional.isPresent()) {
            Applicant updated = modelMapper.map(applicantPutModel, Applicant.class);

            Optional<Position> optionalPosition = positionService.get(applicantPutModel.getPositionId());
            if (!optionalPosition.isPresent())
                return new ResponseEntity<>(new ErrorResponse(EXISTING_MAIL), HttpStatus.NOT_FOUND);

            updated.setPosition(optionalPosition.get());
            updated.setDateTime(optional.get().getDateTime());
            updated.setId(id);

            Optional<Applicant> optionalApplicant = applicantService.update(updated);

            return optionalApplicant.<HttpEntity>map(a -> ResponseEntity.ok(modelMapper.map(a, ApplicantGetModel.class)))
                    .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(BAD_REQUEST), HttpStatus.BAD_REQUEST));
        }
        return new ResponseEntity<>(new ErrorResponse(RESOURCE_NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Applicant> optional = applicantService.get(id);

        return optional.<HttpEntity>map(a -> {
            fileUploadService.deleteUploadedFiles(id);
            applicantService.delete(id);
            return ResponseEntity.noContent().build();
        })
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(RESOURCE_NOT_FOUND), HttpStatus.NOT_FOUND));
    }
}