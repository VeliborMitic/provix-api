package com.proint.provix.controller;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.FileUpload;
import com.proint.provix.exception.ErrorResponse;
import com.proint.provix.model.upload.FileGetModel;
import com.proint.provix.service.ApplicantService;
import com.proint.provix.service.impl.FileUploadServiceImpl;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/applicants")
public class FileUploadController {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
    private FileUploadServiceImpl fileUploadService;
    private ApplicantService applicantService;
    private ModelMapper modelMapper;
    private static final String BAD_REQUEST = "Bad Request";
    private static final String NOT_EXIST = "Aplicant with used id don't exist";
    private static final String TOO_MANY_FILES = "Allowed max 5 files";
    private static final String TYPE_NOT_ALLOWED = " upload type is not allowed";
    private static final String NOT_FOUND = "Applicant not found";
    private static final String FILE_NOT_FOUND = "File not found";
    private static final String FILE_SIZE_EXCEEDED = " -> allowed file size 3MB exceeded";
    private static final Long MAX_ALLOWED_FILE_SIZE_3MB = 3145728L;

    private static final List<String> ALLOWED_MIME_TYPES =
            Arrays.asList("application/pdf",
                    "image/jpeg",
                    "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "application/vnd.oasis.opendocument.text",
                    "image/png",
                    "application/zip");

    @Autowired
    public FileUploadController(FileUploadServiceImpl fileUploadService,
                                ApplicantService applicantService,
                                ModelMapper modelMapper) {
        this.fileUploadService = fileUploadService;
        this.applicantService = applicantService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}/files")
    public HttpEntity browseApplicantFiles(@PathVariable("id") Integer applicantId) {
        Optional<Applicant> optional = applicantService.get(applicantId);
        if (optional.isPresent()) {
            Applicant applicant = modelMapper.map(optional.get(), Applicant.class);

            List<FileUpload> applicantFiles = fileUploadService.getAllByApplicantId(applicant.getId());
            List<FileGetModel> fileGetModels = applicantFiles.stream()
                    .map(file -> modelMapper.map(file, FileGetModel.class))
                    .collect(Collectors.toList());
            return ResponseEntity.ok(fileGetModels);
        }
        return new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/{id}/files")
    public HttpEntity uploadFiles(@PathVariable("id") Integer applicantId,
                                  @RequestParam("files") MultipartFile[] files) {
        Optional<Applicant> optional = applicantService.get(applicantId);

        if (!optional.isPresent()) {
            return new ResponseEntity<>(new ErrorResponse(errorMessageBuilder(NOT_EXIST)), HttpStatus.BAD_REQUEST);
        }
        Applicant applicant = modelMapper.map(optional.get(), Applicant.class);

        if (files.length > 5) {
            return new ResponseEntity<>(new ErrorResponse(errorMessageBuilder(TOO_MANY_FILES)), HttpStatus.BAD_REQUEST);
        }

        for (MultipartFile file : files) {
            if (file.getSize() > MAX_ALLOWED_FILE_SIZE_3MB) {
                return new ResponseEntity<>(
                        new ErrorResponse(errorMessageBuilder(file, FILE_SIZE_EXCEEDED)),
                        HttpStatus.BAD_REQUEST);
            }
            if (!ALLOWED_MIME_TYPES.contains(file.getContentType())) {
                return new ResponseEntity<>(
                        new ErrorResponse(errorMessageBuilder(file, TYPE_NOT_ALLOWED)),
                        HttpStatus.BAD_REQUEST);
            }
        }

        for (MultipartFile file : files) {
            Map<Boolean, String> uploadsResultMap = fileUploadService.storeFile(applicant, file);

            if (uploadsResultMap.containsKey(false)) {
                return new ResponseEntity<>(
                        new ErrorResponse(errorMessageBuilder(uploadsResultMap.get(false))),
                        HttpStatus.BAD_REQUEST);
            }
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}/files/{fileId}")
    public HttpEntity downloadFile(@PathVariable("fileId") Integer fileId, HttpServletResponse response) {
        Optional<FileUpload> optional = fileUploadService.get(fileId);

        if (optional.isPresent()) {
            File file = new File(optional.get().getDownloadPath() + File.separator + optional.get().getFileName());
            if (file.exists()) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
                response.setContentLength((int) file.length());

                try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
                    FileCopyUtils.copy(inputStream, response.getOutputStream());
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return new ResponseEntity<>(new ErrorResponse(FILE_NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    private String errorMessageBuilder(MultipartFile file, String reason) {
        StringJoiner joiner = new StringJoiner(", ");
        return joiner.add(BAD_REQUEST)
                .add(StringUtils.cleanPath(file.getOriginalFilename()))
                .add(reason)
                .toString();
    }

    private String errorMessageBuilder(String reason) {
        StringJoiner joiner = new StringJoiner(", ");
        return joiner.add(BAD_REQUEST)
                .add(reason)
                .toString();
    }
}
