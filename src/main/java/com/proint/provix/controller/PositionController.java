package com.proint.provix.controller;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.exception.ErrorResponse;
import com.proint.provix.model.applicant.ApplicantGetModel;
import com.proint.provix.model.position.PositionGetModel;
import com.proint.provix.model.position.PositionPostModel;
import com.proint.provix.service.ApplicantService;
import com.proint.provix.service.PositionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/positions")
public class PositionController {

    private PositionService positionService;
    private ApplicantService applicantService;
    private ModelMapper modelMapper;
    private static final String POSITIONS_URI = "/api/v1/positions";
    private static final String NOT_FOUND = "Position not found";

    @Autowired
    public PositionController(PositionService positionService,
                              ModelMapper modelMapper,
                              ApplicantService applicantService) {
        this.positionService = positionService;
        this.modelMapper = modelMapper;
        this.applicantService = applicantService;
    }

    @SuppressWarnings("unchecked")
    @GetMapping
    public HttpEntity<Page<PositionGetModel>> get(
            @RequestParam(value = "search", required = false) String search, Pageable pageable) {
        Optional<Page<Position>> optionalPage;

        if (search != null) {
            optionalPage = positionService.get(search, pageable);
        } else {
            optionalPage = positionService.get(pageable);
        }

        return optionalPage.map(
                page -> {
                    List<PositionGetModel> positions = page.stream()
                            .map(p -> modelMapper.map(p, PositionGetModel.class))
                            .collect(Collectors.toList());
                    return new ResponseEntity(new PageImpl(positions, page.getPageable(),
                            page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.ok(new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Position> optionalPosition = positionService.get(id);

        return optionalPosition.map(position -> ResponseEntity.ok(modelMapper.map(position, PositionGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public HttpEntity<PositionGetModel> create(@RequestBody PositionPostModel positionPostModel) {
        Position mappedPosition = modelMapper.map(positionPostModel, Position.class);
        Optional<Position> queriedByName = positionService.getByName(mappedPosition.getName());

        if (queriedByName.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        Optional<Position> optionalPosition = positionService.create(mappedPosition);

        return optionalPosition.map(position -> ResponseEntity.created(URI.create(POSITIONS_URI)).body(modelMapper.map(position, PositionGetModel.class)))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }


    @PutMapping("/{id}")
    public HttpEntity<PositionGetModel> update(@PathVariable("id") Integer id, @RequestBody PositionPostModel positionPostModel) {
        Optional<Position> optionalPosition = positionService.get(id);

        if (!optionalPosition.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Position mappedPosition = modelMapper.map(positionPostModel, Position.class);
        mappedPosition.setId(id);

        if (positionService.existsByNameAndDifferentId(positionPostModel.getName(), id)) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        optionalPosition = positionService.update(mappedPosition);

        return optionalPosition.map(position -> ResponseEntity.ok(modelMapper.map(position, PositionGetModel.class)))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }


    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Position> optionalPosition = positionService.get(id);
        if (!optionalPosition.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        positionService.delete(id);

        return ResponseEntity.ok().build();
    }

    @SuppressWarnings({"unchecked"})
    @GetMapping("/{id}/applicants")
    public HttpEntity get(@PathVariable("id") Integer id,
                          Pageable pageable) {
        Optional<Position> optionalPosition = positionService.get(id);
        if (!optionalPosition.isPresent()) {
            return new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND);
        }
        Optional<Page<Applicant>> optionalPage = applicantService.getAllByPositionId(id, pageable);

        return optionalPage.map(
                page -> {
                    List<ApplicantGetModel> a = page.stream()
                            .map(applicant -> modelMapper.map(applicant, ApplicantGetModel.class))
                            .collect(Collectors.toList());
                    return new ResponseEntity<>(new PageImpl(a, page.getPageable(),
                            page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.ok(new PageImpl(new ArrayList(), pageable, 0)));
    }
}
