package com.proint.provix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvixApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvixApiApplication.class, args);
    }

}
