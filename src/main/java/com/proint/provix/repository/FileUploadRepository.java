package com.proint.provix.repository;

import com.proint.provix.entity.FileUpload;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileUploadRepository extends BaseRepository<FileUpload> {

    List<FileUpload> findAllByApplicantId(Integer id);

    Optional<FileUpload> findByFileName(String name);
}
