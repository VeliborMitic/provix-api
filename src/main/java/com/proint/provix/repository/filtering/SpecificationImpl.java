package com.proint.provix.repository.filtering;

import com.proint.provix.entity.BaseEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.time.format.DateTimeParseException;

public class SpecificationImpl<T extends BaseEntity> implements Specification<T> {

    private SearchCriteria searchCriteria;

    SpecificationImpl(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        try {
            if (searchCriteria.getOperation().equalsIgnoreCase("~")) {
                if (root.get(searchCriteria.getKey()).getJavaType() == String.class) {
                    return criteriaBuilder.like(root.get(searchCriteria.getKey()),
                            "%" + searchCriteria.getValue() + "%");
                } else {
                    return criteriaBuilder.equal(root.get(searchCriteria.getKey()), searchCriteria.getValue());
                }
            } else if (searchCriteria.getOperation().equalsIgnoreCase(">")) {
                if (root.get(searchCriteria.getKey()).getJavaType() == Instant.class) {
                    return criteriaBuilder.greaterThanOrEqualTo(
                            root.get(searchCriteria.getKey()), Instant.parse(searchCriteria.getValue().toString()));
                } else {
                    return criteriaBuilder.greaterThanOrEqualTo(
                            root.get(searchCriteria.getKey()), searchCriteria.getValue().toString());
                }
            } else if (searchCriteria.getOperation().equalsIgnoreCase("<")) {
                if (root.get(searchCriteria.getKey()).getJavaType() == Instant.class) {
                    return criteriaBuilder.lessThanOrEqualTo(
                            root.get(searchCriteria.getKey()), Instant.parse(searchCriteria.getValue().toString()));
                } else {
                    return criteriaBuilder.lessThanOrEqualTo(
                            root.get(searchCriteria.getKey()), searchCriteria.getValue().toString());
                }
            }

            return null;
        } catch (NumberFormatException | DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please enter a valid number. If attempting " +
                    "to enter a date, please input a valid date-time value using ISO-8601 representation, e.g. '2019-01-01T23:59:59Z'.");
        }
    }
}