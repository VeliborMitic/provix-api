package com.proint.provix.repository.filtering;

import com.proint.provix.entity.BaseEntity;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpecificationBuilder<T extends BaseEntity> {
    private final List<SearchCriteria> parameters;

    public SpecificationBuilder() {
        parameters = new ArrayList<>();
    }

    public void with(String key, String operation, Serializable value) {
        parameters.add(new SearchCriteria(key, operation, value));
    }

    @SuppressWarnings("unchecked")
    public Specification<T> build() {
        if (parameters.isEmpty()) {
            return null;
        }

        List<Specification> specs = parameters.stream()
                .map(SpecificationImpl::new)
                .collect(Collectors.toList());

        Specification result = specs.get(0);

        for (int i = 1; i < parameters.size(); i++) {
            result = Specification.where(result).and(new SpecificationImpl<>(parameters.get(i)));
        }

        return result;
    }
}