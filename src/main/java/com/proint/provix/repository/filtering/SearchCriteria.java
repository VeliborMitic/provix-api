package com.proint.provix.repository.filtering;

import java.io.Serializable;

public class SearchCriteria implements Serializable {
    private String key;
    private String operation;
    private Serializable value;


    SearchCriteria(String key, String operation, Serializable value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    Serializable getValue() {
        return value;
    }

    public void setValue(Serializable value) {
        this.value = value;
    }
}