package com.proint.provix.repository;

import com.proint.provix.entity.User;

public interface UserRepository extends BaseRepository<User> {
}