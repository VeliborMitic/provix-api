package com.proint.provix.repository;

import com.proint.provix.entity.Applicant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicantRepository extends BaseRepository<Applicant> {

    Optional<Applicant> findByEmail(String email);

    Page<Applicant> findAllByPositionId(Integer id, Pageable pageable);
}