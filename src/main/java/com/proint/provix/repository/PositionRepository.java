package com.proint.provix.repository;

import com.proint.provix.entity.Position;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PositionRepository extends BaseRepository<Position> {

    Optional<Position> findByNameIs(String name);

    @Query("SELECT (COUNT(p) > 0) FROM Position p WHERE (p.name = :myName) AND p.id <> :myId")
    Boolean existsByNameAndDifferentId(String myName, Integer myId);
}