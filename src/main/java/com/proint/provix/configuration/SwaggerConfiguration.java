package com.proint.provix.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private ApiInfo metaInfo() {
        return new ApiInfoBuilder()
                .title("Provix Backend API")
                .description("Prime Software Internship project")
                .termsOfServiceUrl("http:/www.prime.com/terms")
                .version("v1")
                .contact(new Contact("Provix Team", "https://www.primeinternship.com", "primeinternship@prime.com"))
                .build();
    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(metaInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.proint.provix.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(true)
                .apiInfo(metaInfo())
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext));
    }

    private SecurityReference securityReference = SecurityReference.builder()
            .reference("apiKey")
            .scopes(new AuthorizationScope[0])
            .build();

    private SecurityContext securityContext = SecurityContext.builder()
            .securityReferences(Collections.singletonList(securityReference))
            .build();

    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }
}
