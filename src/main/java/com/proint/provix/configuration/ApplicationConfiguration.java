package com.proint.provix.configuration;

import com.proint.provix.entity.Applicant;
import com.proint.provix.entity.Position;
import com.proint.provix.model.applicant.ApplicantPostModel;
import com.proint.provix.model.position.PositionPostModel;
import com.proint.provix.repository.filtering.SpecificationBuilder;
import com.proint.provix.security.filter.JwtAuthorizationFilter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public JwtAuthorizationFilter jwtAuthorizationFilter() {
        return new JwtAuthorizationFilter();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.createTypeMap(PositionPostModel.class, Position.class)
                .addMappings(mapper -> {
                    mapper.using(stringInstantConverter())
                            .map(PositionPostModel::getStartDate, Position::setStartDate);
                    mapper.using(stringInstantConverter())
                            .map(PositionPostModel::getEndDate, Position::setEndDate);
                });
        modelMapper.createTypeMap(ApplicantPostModel.class, Applicant.class)
                .addMappings(mapper -> mapper.using(integerToNullConverter())
                        .map(ApplicantPostModel::getPositionId, Applicant::setId));
        return modelMapper;
    }

    private Converter<String, Instant> stringInstantConverter() {
        return mappingContext -> {
            if (mappingContext.getSource() != null) {
                return Instant.parse(mappingContext.getSource()).truncatedTo(ChronoUnit.SECONDS);
            }
            return null;
        };
    }

    @Bean
    public SpecificationBuilder specificationBuilder() {
        return new SpecificationBuilder();
    }

    private Converter<Integer, Integer> integerToNullConverter() {
        return mappingContext -> null;

    }
}