package com.proint.provix.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileUploadConfiguration {
    @Value("${root-folder}")
    private String rootFolder;

    public String getRootFolder() {
        return rootFolder;
    }
}
