package com.proint.provix.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.proint.provix.security.JsonWebTokenProvider;
import com.proint.provix.security.filter.JwtAuthenticationFilter;
import com.proint.provix.security.filter.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private JsonWebTokenConfiguration configuration;
    private JwtAuthorizationFilter jwtAuthorizationFilter;
    private ObjectMapper objectMapper;
    private JsonWebTokenProvider jsonWebTokenProvider;

    @Autowired
    public SecurityConfiguration(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder,
                                 JsonWebTokenConfiguration configuration,
                                 JwtAuthorizationFilter jwtAuthorizationFilter,
                                 ObjectMapper objectMapper,
                                 JsonWebTokenProvider jsonWebTokenProvider) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.configuration = configuration;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
        this.objectMapper = objectMapper;
        this.jsonWebTokenProvider = jsonWebTokenProvider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                .addFilter(new JwtAuthenticationFilter(
                        objectMapper,
                        jsonWebTokenProvider,
                        authenticationManager(),
                        configuration
                ))
                .addFilterAfter(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        "/",
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/api/v1/positions",
                        "/api/v1/positions/*"
                )
                .permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/v1/applicants",
                        "/api/v1/applicants/*",
                        "/api/v1/applicants/**/files"
                )
                .permitAll()
                .antMatchers(HttpMethod.POST, configuration.getEntryPoint()).permitAll()
                .anyRequest().authenticated();
    }
}